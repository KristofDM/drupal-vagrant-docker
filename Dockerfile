FROM kristofdm/drupal:vagrant     
MAINTAINER Kristof De Middelaer "kristof.de.middelaer@gmail.com"

RUN echo 'root:vagrant' |chpasswd

EXPOSE 22
CMD /start.sh
