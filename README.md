# README #

## An example drupal website using vagrant + docker ##

### Requirements ###
* Vagrant >=1.6.0 (http://www.vagrantup.com/)
* Docker (https://docker.com/)


### How do I get set up? ###

1. git clone https://bitbucket.org/KristofDM/drupal-vagrant-docker
2. cd drupal-vagrant-docker
3. vagrant up --provider=docker

### How can I see my website? ###

Navigate to the correct ip address in your favorite browser.
How can you find the ip address?

1. vagrant ssh 
2. ifconfig

### How can I edit my website? ### 

Just edit the files in the html folder